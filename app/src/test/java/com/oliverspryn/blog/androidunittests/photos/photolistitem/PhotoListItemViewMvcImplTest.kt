package com.oliverspryn.blog.androidunittests.photos.photolistitem

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.never
import com.nhaarman.mockito_kotlin.spy
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import com.oliverspryn.blog.androidunittests.R
import com.oliverspryn.blog.androidunittests.photos.PhotosModel
import com.oliverspryn.blog.androidunittests.wrappers.PicassoForwarder
import com.squareup.picasso.Picasso
import com.squareup.picasso.RequestCreator
import com.winterbe.expekt.expect
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe

class PhotoListItemViewMvcImplTest : Spek({

    describe("The PhotoListItemViewMvcImpl") {

        var mockPhoto: AppCompatImageView? = null
        var mockPhotoId: AppCompatTextView? = null
        var mockPicassoForwarder: PicassoForwarder? = null
        var mockRootView: View? = null
        var mockRow: ConstraintLayout? = null
        var mockTitle: AppCompatTextView? = null

        var uut: PhotoListItemViewMvcImpl? = null

        beforeEachTest {

            mockPhoto = mock()
            mockPhotoId = mock()
            mockPicassoForwarder = mock()
            mockRow = mock()
            mockTitle = mock()

            mockRootView = mock {
                on { findViewById<AppCompatImageView>(R.id.photo) } doReturn mockPhoto
                on { findViewById<AppCompatTextView>(R.id.photo_id) } doReturn mockPhotoId
                on { findViewById<ConstraintLayout>(R.id.photo_row) } doReturn mockRow
                on { findViewById<AppCompatTextView>(R.id.title) } doReturn mockTitle
            }

            val mockParent: ViewGroup = mock()
            val mockInflater: LayoutInflater = mock {
                on { inflate(R.layout.photo_list_item_fragment, mockParent, false) } doReturn mockRootView
            }

            uut = PhotoListItemViewMvcImpl(
                inflater = mockInflater,
                parent = mockParent,
                picassoForwarder = mockPicassoForwarder!!
            )
        }

        describe("when init") {

            it("inflates the layout and sets it as the root view") {
                expect(uut?.rootView).to.equal(mockRootView)
            }

            it("obtains a reference to the photo") {
                expect(uut?.photo).to.equal(mockPhoto)
            }

            it("obtains a reference to the photo ID") {
                expect(uut?.photoId).to.equal(mockPhotoId)
            }

            it("obtains a reference to the containing row") {
                expect(uut?.row).to.equal(mockRow)
            }

            describe("that row") {
                it("has a click listener") {
                    verify(mockRow)?.setOnClickListener(uut!!)
                }
            }

            it("obtains a reference to the title") {
                expect(uut?.title).to.equal(mockTitle)
            }
        }

        describe("when bindPhoto") {

            val model = PhotosModel(
                id = 42,
                thumbnailUrl = "https://ddg.co/",
                title = "DuckDuckGo",
                url = "https://duckduckgo.com/"
            )

            var mockPicasso: Picasso? = null
            var mockRequestCreator: RequestCreator? = null

            beforeEachTest {
                mockPicasso = mock()
                mockRequestCreator = mock()

                whenever(mockPicasso?.load("https://ddg.co/")).thenReturn(mockRequestCreator)
                whenever(mockPicassoForwarder?.get()).thenReturn(mockPicasso)
                whenever(mockRequestCreator?.placeholder(android.R.color.darker_gray)).thenReturn(mockRequestCreator)
                whenever(mockRequestCreator?.fit()).thenReturn(mockRequestCreator)

                uut?.bindPhoto(model)
            }

            it("saves a reference to that data model") {
                expect(uut?.data).to.equal(model)
            }

            it("sets the ID of the photo") {
                verify(mockPhotoId)?.text = "42"
            }

            it("sets the title") {
                verify(mockTitle)?.text = "DuckDuckGo"
            }

            it("loads the requested thumbnail into the image view using the fit scaling technique and a gray placeholder") {
                verify(mockPicasso)?.load("https://ddg.co/")
                verify(mockRequestCreator)?.placeholder(android.R.color.darker_gray)
                verify(mockRequestCreator)?.fit()
                verify(mockRequestCreator)?.into(mockPhoto)
            }
        }

        describe("when onClick") {

            var listener1: PhotoListItemViewMvc.Listener? = null
            var listener2: PhotoListItemViewMvc.Listener? = null

            beforeEachTest {
                listener1 = spy(object : PhotoListItemViewMvc.Listener {
                    override fun onPhotoTap(photoModel: PhotosModel) = Unit
                })

                listener2 = spy(object : PhotoListItemViewMvc.Listener {
                    override fun onPhotoTap(photoModel: PhotosModel) = Unit
                })

                uut?.registerListener(listener1!!)
                uut?.registerListener(listener2!!)
            }

            describe("and there is cached data") {

                beforeEachTest {
                    uut?.data = mock()
                    uut?.onClick(mockRow)
                }

                it("calls onPhotoTap for all registered listeners") {
                    verify(listener1)?.onPhotoTap(uut?.data!!)
                    verify(listener2)?.onPhotoTap(uut?.data!!)
                }
            }

            describe("and there is not any cached data") {

                beforeEachTest {
                    uut?.data = null
                    uut?.onClick(mockRow)
                }

                it("does not call onPhotoTap for all registered listeners") {
                    verify(listener1, never())?.onPhotoTap(any())
                    verify(listener2, never())?.onPhotoTap(any())
                }
            }

            afterEachTest {
                uut?.unregisterListener(listener1!!)
                uut?.unregisterListener(listener2!!)
            }
        }
    }
})
