package com.oliverspryn.blog.androidunittests.wrappers

import com.squareup.picasso.Picasso
import javax.inject.Inject

class PicassoForwarder @Inject constructor() {
    fun get(): Picasso = Picasso.get()
}
