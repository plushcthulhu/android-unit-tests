package com.oliverspryn.blog.androidunittests.dagger.modules

import android.content.Context
import android.view.LayoutInflater
import com.oliverspryn.blog.androidunittests.mvc.ViewMvcFactory
import dagger.Module
import dagger.Provides

@Module
class MvcModule {

    @Provides
    fun provideLayoutInflater(
        context: Context
    ): LayoutInflater = LayoutInflater.from(
        context
    )

    @Provides
    fun provideViewMvcFactory(
        layoutInflater: LayoutInflater
    ) = ViewMvcFactory(
        layoutInflater = layoutInflater
    )
}
