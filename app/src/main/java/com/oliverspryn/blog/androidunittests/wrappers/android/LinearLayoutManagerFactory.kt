package com.oliverspryn.blog.androidunittests.wrappers.android

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager

class LinearLayoutManagerFactory {
    fun newInstance(context: Context) = LinearLayoutManager(context)
}
