package com.oliverspryn.blog.androidunittests.wrappers

import com.oliverspryn.blog.androidunittests.mvc.ViewMvcFactory
import com.oliverspryn.blog.androidunittests.photos.PhotosAdapter
import com.oliverspryn.blog.androidunittests.photos.PhotosModel
import com.oliverspryn.blog.androidunittests.photos.photolistitem.PhotoListItemViewMvc

class PhotosFactory {

    fun newAdapterInstance(
        listener: PhotoListItemViewMvc.Listener,
        photos: List<PhotosModel> = emptyList(),
        picassoForwarder: PicassoForwarder,
        viewMvcFactory: ViewMvcFactory
    ) = PhotosAdapter(
        listener,
        photos,
        picassoForwarder,
        viewMvcFactory
    )
}
