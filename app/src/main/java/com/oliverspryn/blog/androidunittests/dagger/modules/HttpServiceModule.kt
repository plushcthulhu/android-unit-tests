package com.oliverspryn.blog.androidunittests.dagger.modules

import com.oliverspryn.blog.androidunittests.photos.PhotosApiService
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class HttpServiceModule {
    @Provides
    @Singleton
    fun providePhotosApiService(): PhotosApiService = Retrofit
        .Builder()
        .baseUrl("https://jsonplaceholder.typicode.com/")
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())
        .client(
            OkHttpClient.Builder()
            .connectTimeout(5, TimeUnit.SECONDS)
            .readTimeout(5, TimeUnit.SECONDS)
            .build())
        .build()
        .create(PhotosApiService::class.java)
}
